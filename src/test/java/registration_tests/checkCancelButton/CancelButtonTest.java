package registration_tests.checkCancelButton;

import data_provider.DataProviderExcel;
import org.testng.annotations.Test;
import page_objects.RegistrationPage;

import java.util.HashMap;

public class CancelButtonTest {
    @Test(dataProvider = DataProviderExcel.POSITIVE_REGISTRATION_DATA, dataProviderClass = DataProviderExcel.class,
            description = "Registration test")
    public void checkCancelButtonTest(HashMap<String, String> dataExcel) {
        new RegistrationPage()
                .openPage()
                .waitForPageLoad()
                .enterFirstAndLastName(dataExcel.get("FirstName"), dataExcel.get("LastName"))
                .clickCancelButton()
                .checkCancel();
    }
}
