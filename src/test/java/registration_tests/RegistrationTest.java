package registration_tests;

import base_test.BaseTest;
import data_provider.DataProviderExcel;
import org.testng.annotations.Test;
import page_objects.HomePage;
import page_objects.RegistrationPage;

import java.util.HashMap;

public class RegistrationTest extends BaseTest {
    @Test(dataProvider = DataProviderExcel.POSITIVE_REGISTRATION_DATA, dataProviderClass = DataProviderExcel.class,
            description = "Registration test")
    public void registrationTest(HashMap<String, String> dataExcel) {
        new RegistrationPage()
                .openPage()
                .waitForPageLoad()
                .registerAsUser(dataExcel.get("FirstName"), dataExcel.get("LastName"));

        new HomePage()
                .waitForPageLoad()
                .checkRegistration(dataExcel.get("FirstName"), dataExcel.get("LastName"));
    }
}
