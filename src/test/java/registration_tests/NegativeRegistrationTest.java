package registration_tests;

import base_test.BaseTest;
import data_provider.DataProviderExcel;
import org.testng.annotations.Test;
import page_objects.RegistrationPage;

import java.util.HashMap;

public class NegativeRegistrationTest extends BaseTest {
    @Test(dataProvider = DataProviderExcel.NEGATIVE_REGISTRATION_DATA, dataProviderClass = DataProviderExcel.class,
            description = "Registration test")
    public void registrationTest(HashMap<String, String> dataExcel) {
        new RegistrationPage()
                .openPage()
                .waitForPageLoad()
                .registerAsUser(dataExcel.get("FirstName"), dataExcel.get("LastName"))
                .waitForPageLoad()
                .checkThatRegistrationNotPassed();
    }
}
