package base_test;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.clearBrowserLocalStorage;
import static com.codeborne.selenide.Selenide.close;

public class BaseTest {
    @Parameters("browser")
    @BeforeTest
    public void setup(@Optional("chrome") String browser) {
        if (browser.equalsIgnoreCase("chrome")) {
            Configuration.browser = "chrome";
        } else if (browser.equalsIgnoreCase("firefox")) {
            Configuration.browser = "firefox";
        } else if (browser.equalsIgnoreCase("ie")) {
            Configuration.browser = "ie";
        } else if (browser.equalsIgnoreCase("opera")) {
            Configuration.browser = "opera";
        }

        Configuration.timeout = 3;
        Configuration.startMaximized = true;
    }

    @AfterTest
    public static void tearDown() {
        clearBrowserCookies();
        clearBrowserLocalStorage();
        close();
    }
}
