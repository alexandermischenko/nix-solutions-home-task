package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class RegistrationPage {
    private final SelenideElement REGISTRATION_PAGE_TITLE = $(byValue("Title"));
    private final SelenideElement ERROR_MESSAGE = $(byId("error-message"));
    private final SelenideElement FIRST_NAME_FIELD = $(byId("reg-firstname"));
    private final SelenideElement LAST_NAME_FIELD = $(byId("reg-lastname"));
    private final SelenideElement SAVE_BUTTON = $(byId("registration-submit"));
    private final SelenideElement CANCEL_BUTTON = $(byId("registration-cancel"));

    public RegistrationPage openPage() {
        open("https://www.nix-solutions.test.com/");
        return this;
    }

    public RegistrationPage waitForPageLoad() {
        REGISTRATION_PAGE_TITLE.waitUntil(Condition.visible, 4);
        return this;

    }

    public RegistrationPage registerAsUser(String firstName, String lastName) {
        enterFirstAndLastName(firstName, lastName);
        clickSaveButton();
        return this;
    }

    public void checkCancel() {
        Assert.assertTrue(FIRST_NAME_FIELD.is(Condition.value("")));
        Assert.assertTrue(LAST_NAME_FIELD.is(Condition.value("")));
    }

    public void checkThatRegistrationNotPassed() {
        Assert.assertTrue(
                ERROR_MESSAGE
                        .shouldBe(Condition.visible)
                        .is(Condition.text("Registration is failed")));
    }

    public RegistrationPage enterFirstAndLastName(String firstName, String lastName) {
        enterFirstName(firstName);
        enterLastName(lastName);
        return this;
    }

    public RegistrationPage clickCancelButton() {
        CANCEL_BUTTON
                .waitUntil(Condition.visible, 2)
                .click();
        return this;
    }

    private void enterFirstName(String firstName) {
        FIRST_NAME_FIELD
                .waitUntil(Condition.visible, 2)
                .setValue(firstName);
    }

    private void enterLastName(String lastName) {
        LAST_NAME_FIELD
                .waitUntil(Condition.visible, 2)
                .setValue(lastName);
    }

    private void clickSaveButton() {
        SAVE_BUTTON
                .waitUntil(Condition.visible, 2)
                .click();
    }
}
