package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class HomePage {
    private final SelenideElement HOME_PAGE_TITLE = $(byValue("Title"));
    private final SelenideElement USERNAME = $(byClassName("account-name"));

    public HomePage waitForPageLoad() {
        HOME_PAGE_TITLE.waitUntil(Condition.visible, 4);
        return this;

    }

    public void checkRegistration(String firstName, String lastName) {
        String username = firstName.concat(" " + lastName);

        Assert.assertEquals(USERNAME.getText(), username,
                "Username does not match the expected name");
    }
}
