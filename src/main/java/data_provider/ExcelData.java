package data_provider;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

class ExcelData {

    static Object[][] getRowDataMap(String inputFile, String sheetName) {
        int cellTypeId;
        int j = 0;
        Sheet sheet;
        Map<String, Object> rowDataMap;
        FileInputStream fis;
        Object[][] dataSet;
        Workbook workbook;
//Creating index map
        Map<String, Integer> index = new HashMap<>();
        try {
            fis = new FileInputStream(inputFile);
            workbook = new XSSFWorkbook(fis);
            sheet = workbook.getSheet(sheetName);
//mapping column index with column name.
            Row firstRow = sheet.getRow(0);
            for (Cell cell : firstRow) {
                index.put(cell.getStringCellValue(), j);
                j++;
            }
//get total rows count present in sheet
            int rowCount = sheet.getLastRowNum();
            //initialize the return object
            dataSet = new Object[rowCount][1];

//running for loop for each data_files.excel row and storing values in map
            for (int i = 0; i < rowCount; i++) {
                //initialize data_files.excel row map
                rowDataMap = new HashMap<>();
                Row rowData = sheet.getRow(i + 1);

                for (String key : index.keySet()) {
                    int columnNum = index.get(key);

                    if (Objects.isNull(rowData.getCell(columnNum))) {
                        rowDataMap.put(key, "");
                        continue;
                    }

                    cellTypeId = rowData.getCell(columnNum).getCellType();
                    if (cellTypeId == 0) {
                        DecimalFormat df = new DecimalFormat("#");
                        rowDataMap.put(key, df.format(rowData
                                .getCell(columnNum).getNumericCellValue()));
                    } else if (cellTypeId == 1) {
                        rowDataMap.put(key, rowData.getCell(columnNum)
                                .toString());
                    } else if (cellTypeId == 4) {
                        rowDataMap.put(key, rowData.getCell(columnNum)
                                .getBooleanCellValue());
                    }
                }
                dataSet[i][0] = rowDataMap;
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File hasn't been found. Please specify path");
        } catch (IOException e) {
            throw new RuntimeException("Cannot read file");
        }
        return dataSet;
    }
}