package data_provider;

import org.testng.annotations.DataProvider;

import java.io.File;

public class DataProviderExcel {
    public static final String POSITIVE_REGISTRATION_DATA = "Registration_Positive";
    public static final String NEGATIVE_REGISTRATION_DATA = "Registration_Negative";

    private static final String PATH_TO_EXCEL_BASIC_TEST = "excel_data/data-excel.xlsx";

    private static final File PATH_TO_EXCEL_FOLDER = getPathToFile();

    private DataProviderExcel() {
    }

    private static File getPathToFile() {
        return new File(PATH_TO_EXCEL_BASIC_TEST).getAbsoluteFile();
    }

    @DataProvider(name = POSITIVE_REGISTRATION_DATA)
    public static Object[][] PositiveRegistrationData() {
        return ExcelData.getRowDataMap(
                PATH_TO_EXCEL_FOLDER.getPath(), POSITIVE_REGISTRATION_DATA);
    }

    @DataProvider(name = NEGATIVE_REGISTRATION_DATA)
    public static Object[][] NegativeRegistrationData() {
        return ExcelData.getRowDataMap(
                PATH_TO_EXCEL_FOLDER.getPath(), NEGATIVE_REGISTRATION_DATA);
    }
}